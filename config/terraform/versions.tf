terraform {

  required_providers {
    aws = {
      version = ">= 3.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}
