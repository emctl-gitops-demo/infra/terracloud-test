[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/emctl-gitops-demo/infra/terracloud-test)

# Terraspace Project

This is a Terraspace project. It contains code to provision Cloud infrastructure built with [Terraform](https://www.terraform.io/) and the [Terraspace Framework](https://terraspace.cloud/) then connect it back to Gitlab. It should also deploy [ArgoCD](https://argoproj.github.io/argo-cd/) which will point to a repo and deploy a number of bootstrap apps such as istio / prometheus

## Deploy

To deploy all the infrastructure stacks:

Run all of the ad hoc pipelines OR use TS_ENV=<Development/Production/AnyTFVar's file>

To deploy individual stacks:

    terraspace up vpc # where vpc is app/stacks/vpc

## Terrafile

To use more modules, add them to the [Terrafile](https://terraspace.cloud/docs/terrafile/).
