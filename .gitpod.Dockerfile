FROM boltops/terraspace:alpine

RUN apk update && \
apk add --no-cache bash curl git && \
versions="0.13.6" && \
git clone https://github.com/tfutils/tfenv.git /.tfenv && for version in ${versions}; do /.tfenv/bin/tfenv install ${version}; done && \
/.tfenv/bin/tfenv use ${version} && \
echo 'PATH=/.tfenv/bin:${PATH}' >> ~/.bashrc && \
. ~/.bashrc

# rm -rf /var/cache/apk/*


# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
# RUN brew install bastet
#
# More information: https://www.gitpod.io/docs/config-docker/
