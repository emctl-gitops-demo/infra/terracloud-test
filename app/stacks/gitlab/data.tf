data "kubernetes_secret" "gitlab-admin-token" {
  metadata {
    name      = "${kubernetes_service_account.gitlab-admin.default_secret_name}"
    namespace = "kube-system"
  }
}

data "gitlab_group" "gitops-demo-apps" {
  full_path = "emctl-gitops-demo/apps"
}

data "gitlab_projects" "cluster-management-search" {
  # Returns a list of matching projects. limit to 1 result matching "cluster-management"
  group_id            = data.gitlab_group.gitops-demo-apps.id
  simple              = true
  search              = "cluster-management"
  per_page            = 1
  max_queryable_pages = 1
}

data "aws_eks_cluster" "eks" {
  name  = var.cluster_name
}

data "aws_eks_cluster_auth" "eks" {
  name  = var.cluster_name
}