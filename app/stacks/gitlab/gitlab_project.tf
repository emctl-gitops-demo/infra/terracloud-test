resource "gitlab_group_cluster" "aws_cluster" {
  group                 = data.gitlab_group.gitops-demo-apps.id
  name                  = var.cluster_name
  domain                = "eks.emctl-gitops-demo.com"
  kubernetes_api_url    = data.aws_eks_cluster.eks.endpoint
  kubernetes_token      = data.kubernetes_secret.gitlab-admin-token.data.token
  environment_scope     = "aws/${var.stage}"
  kubernetes_ca_cert    = trimspace(base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data))
  management_project_id = data.gitlab_projects.cluster-management-search.projects.0.id
}
