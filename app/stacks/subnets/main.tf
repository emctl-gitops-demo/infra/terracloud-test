# This is where you put your resource declaration
  module "label" {
    source     = "../../../../../vendor/modules/labels"
    namespace  = var.namespace
    name       = var.name
    stage      = var.stage
    delimiter  = var.delimiter
    attributes = compact(concat(var.attributes, list("cluster")))
    tags       = var.tags
  }

  locals {
    tags = merge(var.tags, map("kubernetes.io/cluster/${module.label.id}", "shared"))
  }


  module "subnets" {
    source               = "../../../../../vendor/modules/subnets"
    availability_zones   = var.availability_zones
    namespace            = var.namespace
    stage                = var.stage
    name                 = var.name
    attributes           = var.attributes
    vpc_id               = var.vpc_id
    igw_id               = var.igw_id
    cidr_block           = var.vpc_cidr_block
    nat_gateway_enabled  = false
    nat_instance_enabled = false
    tags                 = local.tags
  }
