region             = "<%= expansion(':REGION') %>"
availability_zones = ["<%= expansion(':REGION') %>a", "<%= expansion(':REGION') %>b", "<%= expansion(':REGION') %>c"]
namespace          = "emctl"
tags               = {name="<%= expansion(':REGION-:ENV-:BUILD_DIR-terraspace') %>"}
vpc_id             = <%= output('vpc.vpc_id') %>
igw_id             = <%= output('vpc.igw_id') %>
vpc_cidr_block     = <%= output('vpc.vpc_cidr_block') %>
name               = "eks-test"
