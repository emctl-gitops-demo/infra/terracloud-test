# This is where you put your variables declaration

variable "availability_zones" {
  type = list(string)
}

variable "vpc_id" {
  description = "VPC to launch instance in"
  type        = string
  default     = null
}

variable "igw_id" {
  description = "internet gw id"
  type        = string
  default     = null
}

variable "vpc_cidr_block" {
  description = "VPC cidr range"
  type        = string
  default     = null
}


