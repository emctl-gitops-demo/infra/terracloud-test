variable "region" {
  type = string
}

variable "cidr_block" {
  type        = string
  description = "VPC cidr for the EKS cluster"
  default     = "172.16.0.0/16"
}
