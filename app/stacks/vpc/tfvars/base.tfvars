region    = "<%= expansion(':REGION') %>"
tags      = { name = "<%= expansion(':REGION-:ENV-:BUILD_DIR-terraspace') %>" }
namespace = "emctl"
name      = "eks-test"
