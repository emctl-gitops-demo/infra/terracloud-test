data "aws_eks_cluster" "eks" {
  name  = var.cluster_name
}

data "aws_eks_cluster_auth" "eks" {
  name  = var.cluster_name
}

provider "helm" {
  kubernetes {
    token                  = data.aws_eks_cluster_auth.eks.token
    host                   = data.aws_eks_cluster.eks.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority.0.data)
  }
}

resource "helm_release" "argo-cd" {
  name       = "argocd"
  repository = "https://argoproj.github.io/argo-helm" 
  chart      = "argo-cd"
  version    = "2.10.0"
  namespace  = "argocd"
  create_namespace = true

  values = [
    "${file("./values.yaml")}"
  ]

}