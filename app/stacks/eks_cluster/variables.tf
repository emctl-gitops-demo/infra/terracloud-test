# This is where you put your variables declaration
variable "region" {
  type        = string
  description = "AWS Region"
}

variable "vpc_id" {
  type        = string
  description = "VPC ID for the EKS cluster"
}

variable "subnet_ids" {
  description = "A list of subnet IDs to launch the cluster in"
  type        = list(string)
}

variable "kubernetes_version" {
  type        = string
  default     = "1.17"
  description = "Desired Kubernetes master version. If you do not specify a value, the latest available version is used"
}

variable "health_check_type" {
  type        = string
  description = "Controls how health checking is done. Valid values are `EC2` or `ELB`"
  default     = "EC2"
}

variable "max_size_node" {
  type        = number
  description = "The maximum size of the autoscale group"
}

variable "min_size_node" {
  type        = number
  description = "The minimum size of the autoscale group"
}

variable "wait_for_capacity_timeout" {
  type        = string
  description = "A maximum duration that Terraform should wait for ASG instances to be healthy before timing out. Setting this to '0' causes Terraform to skip all Capacity Waiting behavior"
  default     = "10m"
}


variable "autoscaling_policies_enabled" {
  type        = bool
  default     = true
  description = "Whether to create `aws_autoscaling_policy` and `aws_cloudwatch_metric_alarm` resources to control Auto Scaling"
}

variable "cpu_utilization_low_threshold_percent" {
  type        = number
  default     = 10
  description = "The value against which the specified statistic is compared"
}

variable "cpu_utilization_high_threshold_percent" {
  type        = number
  default     = 90
  description = "The value against which the specified statistic is compared"
}

variable "public_subnet_ids" {
  description = "A list of subnet IDs to launch the cluster in"
  type        = list(string)
}


variable "instance_type_node" {
  type        = list(string)
  description = "Instance type to launch"
}

variable "resources_to_tag" {
  type        = list(string)
  description = "List of auto-launched resource types to tag. Valid types are \"instance\", \"volume\", \"elastic-gpu\", \"spot-instances-request\"."
  default     = []
}

variable "workername" {
  type        = string
  default     = "test-small"
  description = "Solution name, e.g. 'app' or 'cluster'"
}

variable "desired_size_node" {
  type        = number
  description = "Initial desired number of worker nodes (external changes ignored)"
}

variable "eks_node_enabled" {
  type        = bool
  default     = false
  description = "If true, the node group is enabled" 
}

variable "eks_fargate_enabled" {
  type        = bool
  default     = false
  description = "If true, the fargate worker group is enabled" 
}

variable "capacity_type" {
  type        = string
  default     = "SPOT"
  description = <<-EOT
  Type of capacity associated with the EKS Node Group. Valid values: ON_DEMAND, SPOT. 
  Terraform will only perform drift detection if a configuration value is provided.
  EOT
}