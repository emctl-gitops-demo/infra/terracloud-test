kubernetes_version = "1.18"
stage              = "staging"
workername         = "small"

eks_node_enabled   = "true"
min_size_node      = "1"
max_size_node      = "2"
instance_type_node = ["t3.small"]
desired_size_node  = "2"
