region            = "us-east-1"
namespace         = "emctl"
vpc_id            = <%= output('vpc.vpc_id') %>
subnet_ids        = <%= output('subnets.public_subnet_ids') %>
public_subnet_ids = <%= output('subnets.public_subnet_ids') %>
