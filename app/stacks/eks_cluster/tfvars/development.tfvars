kubernetes_version = "1.18"
stage              = "development"
workername         = "small"

eks_node_enabled   = "true"
min_size_node      = "1"
max_size_node      = "3"
instance_type_node = ["t3.medium"]
desired_size_node  = "2"
capacity_type      = "SPOT"
