# This is where you put your resource declaration

locals {
  eks_worker_ami_name_filter = "amazon-eks-node-${var.kubernetes_version}*"
}

module "eks_cluster" {
  source                     = "../../../../../vendor/modules/eks-cluster"
  region                     = var.region
  namespace                  = var.namespace
  stage                      = var.stage
  name                       = var.name
  attributes                 = var.attributes
  vpc_id                     = var.vpc_id
  subnet_ids                 = var.subnet_ids
  kubernetes_version         = var.kubernetes_version
  oidc_provider_enabled      = false

  workers_role_arns          = [module.eks_node_group.eks_node_group_role_arn]
  workers_security_group_ids = []
}

data "null_data_source" "wait_for_cluster_and_kubernetes_configmap" {
  inputs = {
    cluster_name             = module.eks_cluster.eks_cluster_id
    kubernetes_config_map_id = module.eks_cluster.kubernetes_config_map_id
  }
}

module "eks_node_group" {
  source             = "../../../../../vendor/modules/eks-node-group"
  namespace          = var.namespace
  enabled            = var.eks_node_enabled
  stage              = var.stage
  name               = var.name
  attributes         = var.attributes
  tags               = var.tags
  subnet_ids         = var.public_subnet_ids
  instance_types     = var.instance_type_node
  resources_to_tag   = var.resources_to_tag
  desired_size       = var.desired_size_node
  min_size           = var.min_size_node
  max_size           = var.max_size_node
  cluster_name       = data.null_data_source.wait_for_cluster_and_kubernetes_configmap.outputs["cluster_name"]
  kubernetes_version = var.kubernetes_version
  capacity_type      = var.capacity_type
}
